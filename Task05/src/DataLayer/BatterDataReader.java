package DataLayer;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import Models.Batters;

public class BatterDataReader {

    private static final String CSV_SEPARATOR = ",";

    public static List<Batters> readPlayersFromFile(String filePath) {
        List<Batters> players = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            players = reader.lines()
                    .skip(1) // Skip the first row
                    .map(line -> line.split(CSV_SEPARATOR))
                    .map(BatterDataReader::createPlayerFromData)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return players;
    }

    private static Batters createPlayerFromData(String[] data) {
        String playerName = data[1]; 
        String span = data[2];
        int matches = parseIntegerOrDefault(data[3], 0); 
        int innings = parseIntegerOrDefault(data[4], 0); 
        int playerNumber = parseIntegerOrDefault(data[5], 0); 
        int runs = parseIntegerOrDefault(data[6], 0);
        String highestScore = data[7];
        double average = parseDoubleOrDefault(data[8], 0);
        int ballsFaced = parseIntegerOrDefault(data[9], 0); 
        double strikeRate = parseDoubleOrDefault(data[10], 0); 
        int centuries = parseIntegerOrDefault(data[11], 0); 
        int fifties = parseIntegerOrDefault(data[12], 0); 
        int zeros = parseIntegerOrDefault(data[13], 0); 

        return new Batters(playerName, span, matches, innings, playerNumber, runs, highestScore,
                average, ballsFaced, strikeRate, centuries, fifties, zeros);
    }

    private static int parseIntegerOrDefault(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private static double parseDoubleOrDefault(String value, double defaultValue) {
        if (value.equals("-")) {
            return defaultValue;
        }
        return Double.parseDouble(value);
    }
}
