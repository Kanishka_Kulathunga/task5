package Models;
import java.util.List;
import java.util.stream.Collectors;
import DataLayer.BatterDataReader;

public class Main {

	public static void main(String[] args) {
		List<Batters> players = BatterDataReader
				.readPlayersFromFile("C:/Users/Kanishka/Desktop/CINEC/Advanced Software Engineering/Java/Task05/batters.csv");
				
		System.out.println("---Calculating Total Runs---");
		long startTime = System.currentTimeMillis();
		int totalRunsSequential = players.stream().mapToInt(Batters::getRuns).sum();
		long endTime = System.currentTimeMillis();
		long sequentialTime = endTime - startTime;

		// Calculate total runs using parallel stream
		startTime = System.currentTimeMillis();
		int totalRunsParallel = players.parallelStream().mapToInt(Batters::getRuns).sum();
		endTime = System.currentTimeMillis();
		long parallelTime = endTime - startTime;

		// Print the total runs and execution times
		System.out.println("Total Runs (Sequential): " + totalRunsSequential);
		System.out.println("Total Runs (Parallel): " + totalRunsParallel);
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

		System.out.println();
		System.out.println();
		
		
		System.out.println("---Finding the player with most runs using compareTo method---");
		startTime = System.currentTimeMillis();
		Batters highestScorerSequential = players
				.stream().max(Batters::compareTo)
				.orElse(null);
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		startTime = System.currentTimeMillis();
		Batters highestScorerParallel = players.parallelStream()
				.max(Batters::compareTo)
				.orElse(null);
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		System.out.println("Most Runs (Sequential): " + highestScorerSequential);
		System.out.println("Most Runs (Parallel): " + highestScorerParallel);
		
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

		System.out.println();
		System.out.println();
		
		System.out.println("---Perform sorting based on player name---");
		startTime = System.currentTimeMillis();
		List<Batters> sortedSequential = players.stream()
				.sorted((player1, player2) -> player1.getPlayerName().compareTo(player2.getPlayerName()))
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		// Perform sorting based on player name using parallel stream
		startTime = System.currentTimeMillis();
		List<Batters> sortedParallel = players.parallelStream()
				.sorted((player1, player2) -> player1.getPlayerName().compareTo(player2.getPlayerName()))
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		// Print the execution times
		System.out.println("\nSequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");
		
		System.out.println();
		System.out.println();

		
		System.out.println("---Filtering players with more than 5 centuries---");
		int minCenturies = 5; // Minimum number of centuries

		startTime = System.currentTimeMillis();
		List<Batters> filteredPlayersSequential = players.stream()
				.filter(player -> player.getCenturies() > minCenturies).collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		// Filtering using parallel stream
		startTime = System.currentTimeMillis();
		List<Batters> filteredPlayersParallel = players.parallelStream()
				.filter(player -> player.getCenturies() > minCenturies).collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		// Print the execution times
		System.out.println("\nSequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

	}

}
